codes = true
ignore = { '212/self' }
max_line_length = false
max_cyclomatic_complexity = 15

read_globals = {
  -- WoW API functions.
  'C_AchievementInfo',
  'C_Container',
  'C_CurrencyInfo',
  'C_Item',
  'C_Map',
  'C_MountJournal',
  'C_PetJournal',
  'C_Reputation',
  'C_QuestLog',
  'C_Spell',
  'C_SuperTrack',
  'C_Texture',
  'C_ToyBox',
  'C_TransmogCollection',
  'CLOSE',
  'CloseDropDownMenus',
  'CreateFrame',
  'CreateFromMixins',
  'Enum',
  'GameTooltip',
  'GetAchievementCriteriaInfo',
  'GetAchievementCriteriaInfoByID',
  'GetAchievementInfo',
  'GetAchievementLink',
  'GetAchievementNumCriteria',
  'GetBuildInfo',
  'GetContainerItemID',
  'GetContainerNumSlots',
  'GetItemInfo',
  'GetProfessionInfo',
  'GetProfessions',
  'GetText',
  'IsAltKeyDown',
  'IsShiftKeyDown',
  'MapCanvasDataProviderMixin',
  'MapCanvasPinMixin',
  'NUM_BAG_SLOTS',
  'PlayerHasToy',
  'Settings',
  'ToggleDropDownMenu',
  'TransmogUtil',
  'UIDropDownMenu_AddButton',
  'UIDropDownMenu_AddSpace',
  'UIDropDownMenu_CreateInfo',
  'UiMapPoint',
  'UIParent',
  'UnitFactionGroup',
  'WorldFrame',
  'WorldMapFrame',
}

globals = {
  -- Cache storage.
  'HandyNotes_CollectionCACHE',
  'HandyNotes_CollectionSTATUS',
  --- Addon Compartment.
  'HandyNotes_Collection_OnAddonCompartmentClick',
}
